use std::time;

use crate::mem::Mem;
use crate::display::Display;
use crate::keyboard::Keyboard;

pub struct Bus {
    mem: Mem,
    screen: Display,
    keyboard: Keyboard,
    delay: u8,
    delay_set_time: time::Instant,
}

impl Bus {
    pub fn new() -> Bus {
        Bus {
            mem: Mem::new(),
            screen: Display::new(),
            keyboard: Keyboard::new(),
            delay: 0,
            delay_set_time: time::Instant::now(),
        }
    }

    pub fn get_display_buffer(&self) -> [u8; 64 * 32] {
        self.screen.get_display_buffer()
    }

    pub fn wr_byte_to_mem (&mut self, adress: u16, value: u8) {
        self.mem.write_byte(adress, value);
    }

    pub fn rd_byte_from_mem (&self, adress:u16) -> u8 {
        self.mem.read_byte(adress)
    }

    pub fn update_screen_buffer (&mut self, byte:u8, x:u8, y:u8) -> bool {
        self.screen.update_screen_buffer(byte, x, y)
    }

    pub fn set_key_pressed(&mut self, key_code: u8) {
        self.keyboard.set_key_pressed(key_code);
    }

    pub fn get_key_pressed(&self) -> u8 {
        self.keyboard.get_key_pressed()
    }

    pub fn is_key_pressed(&self, key_code: u8) -> bool {
        self.keyboard.is_key_pressed(key_code)
    }

    pub fn set_delay(&mut self, value:u8){
        self.delay = value;
        self.delay_set_time = time::Instant::now();
    }

    pub fn get_delay(&self) -> u8 {
        let millis = time::Instant::now()-self.delay_set_time;
        let tick = millis / 16;
        if tick > time::Duration::from_millis(self.delay as u64) {
            0
        } else {
            self.delay - tick.as_secs() as u8
        }
    }

    pub fn clear_screen(&mut self) {
        self.screen.clear_screen();
    }
}