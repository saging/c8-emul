extern crate minifb;
extern crate rand;

use std::{thread, time};
use std::fs::File;
use std::io::prelude::*;
use minifb::{Key, KeyRepeat, WindowOptions, Window};

mod cpu;
mod errorhandler;
mod mem;
mod bus;
mod display;
mod keyboard;

use crate::display::Display;
use crate::errorhandler::Status;
use crate::errorhandler::Error;
use crate::errorhandler::Result;

fn minifb_key_to_c8_key(key: Key) -> u8 {
    match key {
        Key::NumLock => 0x1,
        Key::NumPadSlash => 0x2,
        Key::NumPadAsterisk => 0x3,
        Key::NumPadMinus => 0xC,

        Key::NumPad7 => 0x4,
        Key::NumPad8 => 0x5,
        Key::NumPad9 => 0x6,
        Key::NumPadPlus => 0xD,

        Key::NumPad4 => 0x7,
        Key::NumPad5 => 0x8,
        Key::NumPad6 => 0x9,
        Key::NumPadEnter => 0xE,
        
        Key::NumPad1 => 0xA,
        Key::NumPad2 => 0x0,
        Key::NumPad3 => 0xB,
        Key::NumPad0 => 0xF,
        
        _ => u8::max_value()
    }
}

fn main() {
    let mut file = File::open("data/PONG2").unwrap();
    let mut data = Vec::<u8>::new();
    file.read_to_end(&mut data).expect("File not found");

    let mut cpu = cpu::Cpu::new();
    cpu.load_pgrm(&data);

    let mut window = Window::new("C8-emul", 640, 320, WindowOptions::default()).unwrap_or_else(|e| {
        panic!("{}",e);
    });
    let mut buffer: Vec<u32> = vec![0;640*320];

    while window.is_open() && !window.is_key_down(Key::Escape) {
        match cpu.run_instruction() {
            Result::Ok(Status::Terminated) => {
                break;
            }
            Result::Ok(Status::Running) => {
                let screen_buffer = cpu.get_display_buffer();
                for x in 0..640 {
                    let i = x/10;
                    for y in 0..320 {
                        let j = y/10;
                        let id = Display::get_index_from_coord(i, j);
                        let pix = screen_buffer[id];
                        let pix_color = match pix {
                            0 => 0x0,
                            1 => 0xFFFFFF,
                            _ => unreachable!(),
                        };
                        buffer[y * 640 + x] = pix_color;
                    }
                }
                window.update_with_buffer(&buffer).expect("Update screen failed");
                //thread::sleep(time::Duration::from_millis(30));
            }
            Result::Err(Error::UnknownCommand) => {
                panic!("Error: unknown command!");
            }
            Result::Err(Error::LowerStackException) => {
                panic!("Error: Failed to return -> stack empty");
            }
            Result::Err(Error::UpperStackException) => {
                panic!("Error: Failed to call -> out of stack space");
            }
        }

        let keys_pressed = window.get_keys_pressed(KeyRepeat::Yes);
        let key = match keys_pressed {
            Some(keys) => if keys.is_empty() {
                Key::Unknown
            } else {
                keys[0]
            }
            None => Key::Unknown
        };
        let c8key = minifb_key_to_c8_key(key);
        cpu.set_key_pressed(c8key);
    }
}
