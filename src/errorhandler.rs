#[allow(dead_code)]
pub enum Status {
    Terminated,
    Running,
}

#[allow(dead_code)]
pub enum Error {
    UnknownCommand,
    LowerStackException,
    UpperStackException,
}

#[allow(dead_code)]
pub enum Result<T, E> {
    Ok(T),
    Err(E),
}
