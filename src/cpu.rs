use rand::Rng;

use crate::bus::Bus;
use crate::errorhandler::Error;
use crate::errorhandler::Result;
use crate::errorhandler::Status;

#[allow(dead_code)]
pub struct Cpu {
    reg8: [u8; 16],
    regi: u16,
    pc: u16,
    stack: Vec<u16>,
    bus: Bus,
    rng: rand::rngs::ThreadRng,
}

const PROGRAM_START: u16 = 0x200;

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            reg8: [0; 16],
            regi: 0,
            pc: PROGRAM_START,
            stack: Vec::with_capacity(16),
            bus: Bus::new(),
            rng: rand::thread_rng(),
        }
    }

    pub fn load_pgrm(&mut self, prgm: &Vec<u8>) {
        let start = 0x200;
        for i in 0..prgm.len() {
            self.bus.wr_byte_to_mem((start + i) as u16, prgm[i]);
        }
    }

    pub fn run_instruction(&mut self) -> Result<Status, Error> {
        let lb = self.bus.rd_byte_from_mem(self.pc + 1);
        let hb = self.bus.rd_byte_from_mem(self.pc);
        let instruction_code: u16 = (hb as u16) << 8 | lb as u16;

        let x = ((instruction_code & 0xF00) >> 8) as usize;
        let y = ((instruction_code & 0xF0) >> 4) as usize;
        let val16 = instruction_code & 0xFFF;
        let val8 = (instruction_code & 0xFF) as u8;

    /*    println!(
            "{:04X} -> X: {:X} Y: {:X} val8: {:X} val16: {:X}",
            instruction_code, x, y, val8, val16
        );
*/
        match (instruction_code & 0xF000) >> 12 {
            0x0 => match val8 {
                0xEE => {
                    self.pc = self.stack.pop().unwrap();
                },
                0xE0 => {
                    self.bus.clear_screen();
                },
                _ => {
                    return Result::Err(Error::UnknownCommand);
                },
            },
            0x1 => {
                self.pc = val16 - 2;
            },
            0x2 => {
                self.stack.push(self.pc + 2);
                self.pc = val16 - 2;
            },
            0x3 => {
                if self.reg8[x] == val8 {
                    self.pc += 2;
                }
            },
            0x4 => {
                if val8 != self.reg8[x] {
                    self.pc += 2;
                }
            },
            0x5 => {
                if self.reg8[x] == self.reg8[y] {
                    self.pc += 2;
                }
            },
            0x6 => {
                //set register X to val8;
                self.reg8[x] = val8;
            },
            0x7 => {
                self.reg8[x] = self.reg8[x].wrapping_add(val8);
            },
            0x8 => match instruction_code & 0x000F {
                0x0 => {
                    self.reg8[x] = self.reg8[y];
                },
                0x1 => {
                    self.reg8[x] = self.reg8[x] | self.reg8[y];
                },
                0x2 => {
                    self.reg8[x] = self.reg8[x] & self.reg8[y];
                },
                0x3 => {
                    self.reg8[x] = self.reg8[x] ^ self.reg8[y];
                },
                0x4 => {
                    let result = self.reg8[x].overflowing_add(self.reg8[y]);
                    if result.1 {
                        self.reg8[0xF] = 1;
                    } else {
                        self.reg8[0xF] = 0;
                    }
                    self.reg8[x] = result.0;
                },
                0x5 => {
                    let result = self.reg8[x].overflowing_sub(self.reg8[y]);
                    if result.1 {
                        self.reg8[0xF] = 1;
                    } else {
                        self.reg8[0xF] = 0;
                    }
                    self.reg8[x] = result.0;
                },
                0x6 => {
                    self.reg8[0xF] = self.reg8[x] & 0x1;
                    self.reg8[x] = self.reg8[x] >> 1;
                },
                0x7 => {
                    let result = self.reg8[y].overflowing_sub(self.reg8[x]);
                    if result.1 {
                        self.reg8[0xF] = 1;
                    } else {
                        self.reg8[0xF] = 0;
                    }
                    self.reg8[x] = result.0;
                },
                0xE => {
                    self.reg8[0xF] = self.reg8[x] & 0x80;
                    self.reg8[x] = self.reg8[x] >> 1;
                },
                _ => return Result::Err(Error::UnknownCommand),
            },
            0x9 => match instruction_code & 0xF {
                0x0 => {
                    if self.reg8[x] != self.reg8[y] {
                        self.pc += 2;
                    }
                },
                _ => {
                    return Result::Err(Error::UnknownCommand);
                },
            },
            0xA => {
                self.regi = val16;
            },
            0xB => {
                self.pc = val16 + self.reg8[0] as u16;
            },
            0xC => {
                let value: u8 = self.rng.gen();
                self.reg8[x] = val8 & value;
            },
            0xD => {
                let vx = self.reg8[x];
                let vy = self.reg8[y];
                self.draw_sprite(vx, vy, val8 & 0xF);
            },
            0xE => match val8 {
                0x9E => {
                    if !self.bus.is_key_pressed(self.reg8[x]) {
                        self.pc += 2;
                    }
                },
                0xA1 => {
                    if self.bus.is_key_pressed(self.reg8[x]) {
                        self.pc += 2;
                    }
                },
                _ => {
                    return Result::Err(Error::UnknownCommand);
                }
            },
            0xF => match val8 {
                0x07 => {
                    self.reg8[x] = self.bus.get_delay();
                },
                0x0A => {
                    unimplemented!();
                },
                0x15 => {
                    self.bus.set_delay(self.reg8[x]);
                },
                0x18 => {
                    //ignore sound to make the rest
                },
                0x1E => {
                    self.regi += self.reg8[x] as u16;
                },
                0x29 => {
                    self.regi = self.reg8[x] as u16 * 5;
                },
                0x33 => {
                    let mut value = self.reg8[x];
                    self.bus.wr_byte_to_mem(self.regi, value / 100);
                    self.bus.wr_byte_to_mem(self.regi + 1, (value % 100) / 10);
                    self.bus.wr_byte_to_mem(self.regi + 2, value % 10);
                },
                0x55 => {

                }
                0x65 => {
                    for i in 0..x + 1 {
                        self.reg8[i] = self.bus.rd_byte_from_mem(self.regi + i as u16);
                    }
                },
                _ => {
                    return Result::Err(Error::UnknownCommand);
                },
            },
            _ => {
                return Result::Err(Error::UnknownCommand);
            },
        }
        self.pc += 2;
        Result::Ok(Status::Running)
    }

    pub fn get_display_buffer(&self) -> [u8; 64 * 32] {
        self.bus.get_display_buffer()
    }
    #[allow(dead_code)]
    pub fn cpu_dump(&self) {
        for i in 0..self.reg8.len() {
            println!("V{:X}: {},", i, self.reg8[i]);
        }
        println!("I: {},", self.regi);
        println!("PC: {},", self.pc);
        //self.mem.mem_dump();
    }

    pub fn set_key_pressed(&mut self, key_code: u8) {
        self.bus.set_key_pressed(key_code);
    }

    pub fn get_key_pressed(&self) -> u8 {
        self.bus.get_key_pressed()
    }

    pub fn is_key_pressed(&self, key_code: u8) -> bool {
        self.bus.is_key_pressed(key_code)
    }

    fn draw_sprite(&mut self, x: u8, y: u8, h: u8) {
        let mut is_vf_set = false;
        for j in 0..h {
            let line_as_byte = self.bus.rd_byte_from_mem(self.regi + j as u16);
            if self.bus.update_screen_buffer(line_as_byte, x, y + j) {
                is_vf_set = true;
            }
        }
        if is_vf_set {
            self.reg8[0xF] = 1;
        } else {
            self.reg8[0xF] = 0;
        }
    }
}
