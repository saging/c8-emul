pub struct Display {
    screen: [u8;64 * 32]
}

impl Display {
    pub fn new() -> Display {
        Display {
            screen: [0;64 * 32]
        }
    }

    pub fn clear_screen(&mut self) {
        for pixel in self.screen.iter_mut() {
            *pixel = 0;
        }
    }

    pub fn get_display_buffer(&self) -> [u8; 64 * 32] {
        self.screen
    }

    pub fn get_index_from_coord(i: usize, j:usize) -> usize {
        i + j * 64
    }

    pub fn update_screen_buffer(&mut self, byte: u8, x:u8, y:u8) -> bool{
        let mut is_unset = false;
        let mut line = byte;
        let mut i = x as usize;
        let mut j = y as usize;

        for _ in 0..8 {
            i %= 64;
            j %= 32;
            let id = Display::get_index_from_coord(i, j);
            let prev_value = self.screen[id];
            self.screen[id] ^= (line & 0x80) >> 7;

            if prev_value == 1 && self.screen[id] == 0 {
                is_unset = true;
            }
            i+=1;
            line = line << 1;
        }
        is_unset
    }
}