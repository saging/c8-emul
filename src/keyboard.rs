pub struct Keyboard {
    key_pressed: u8
}

impl Keyboard {
    pub fn new() -> Keyboard {
        Keyboard {
            key_pressed: u8::max_value(),
        }
    }

    pub fn is_key_pressed(&self, key_code: u8) -> bool {
        if key_code == u8::max_value() {
            false
        } else {
            key_code == self.key_pressed
        }
    }

    pub fn set_key_pressed(&mut self, key_code: u8) {
        self.key_pressed = key_code;
    }

    pub fn get_key_pressed(&self) -> u8 {
        self.key_pressed
    }
}